# Helper Makefile for building the course support

SLIDES_PDF = $(CURDIR)/cmake.pdf
EXERCISES_PDF = $(CURDIR)/exercices.pdf

.PHONY: all clean cmake-% exercises-%

all: cmake-build exercises-build

cmake-build: $(SLIDES_PDF)

exercises-build: $(EXERCISES_PDF)

clean: cmake-clean exercises-clean

cmake-clean:
	rm -rf build-slides
	rm -f $(SLIDES_PDF)

exercises-clean:
	rm -rf build-exercices
	rm -f $(EXERCISES_PDF)

$(SLIDES_PDF): $(CURDIR)/slides/*.tex
	mkdir -p build-slides
	cd build-slides && cmake ..
	make -C build-slides
	cp build-slides/slides/cmake.pdf $(SLIDES_PDF)

$(EXERCISES_PDF): $(CURDIR)/exercices/exercices.tex
	mkdir -p build-exercices
	cd build-exercices && cmake ../exercices
	make -C build-exercices
	cp build-exercices/exercices.pdf $(EXERCISES_PDF)
