#include "foo.h"
#include <iostream>


Foo::Foo() : m_description("description par defaut")
{
    leak = new char[20];
}

Foo::~Foo()
{
    delete [] leak;
}

std::string Foo::description() const
{
    return m_description;
}

void Foo::setDescription(const std::string & description)
{
    m_description = description;
}
