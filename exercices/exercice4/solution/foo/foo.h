#include <string>

#include "foo_export.h"

class FOO_EXPORT Foo {

public:
    Foo();

    ~Foo();

    void setDescription(const std::string & description);

    std::string description(void) const;

private:
    char * leak;
    std::string m_description;
};
