#include <iostream>
#include "foo.h"
#include "bar.h"

int main (int argc, char * argv[]) {
  Foo * foo = new Foo();
  foo->setDescription("Je suis la lib foo");
  std::cout << "Nouvelle description de foo: " << foo->description() << std::endl;
  
  Bar * bar = new Bar();
  bar->setValue(10.0);
  std::cout << "Nouvelle valeur de Bar: " << bar->value() << std::endl;
  
  delete foo;
  delete bar;
  
  return 0;
}