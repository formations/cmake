// testFooBar.cpp
#include <assert.h>
#include <foo.h>
#include <bar.h>

int main (int argc, char * argv[]) {
    Foo * foo = new Foo();
    assert(foo->description() == "description par defaut");

    foo->setDescription("test");
    assert(foo->description() == "test");

    delete foo;

    Bar * bar = new Bar();
    assert(bar->value() == 0.0);    
    bar->setValue(10.0);
    assert(bar->value() == 10.0);

    delete bar;

    return 0;
}
