#include <iostream>

#include "foo.h"


Foo::Foo() : m_description("description par defaut")
{
    leak = new char[20];
}

std::string Foo::description() const
{
    return m_description;
}

void Foo::setDescription(const std::string & description)
{
    m_description = description;
}
