#include "bar_export.h"

class BAR_EXPORT Bar {

public:
    Bar();

    ~Bar();

    void setValue(const float value);

    float value(void) const;

private:
    float m_value;
};
