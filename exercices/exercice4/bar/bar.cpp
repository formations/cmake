#include "bar.h"

Bar::Bar() : m_value(0.0) {}

Bar::~Bar() {}

float Bar::value() const
{
    return m_value;
}

void Bar::setValue(const float value)
{
    m_value = value;
}
