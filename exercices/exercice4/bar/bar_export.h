#ifndef BAR_EXPORT_H
#define BAR_EXPORT_H

#ifdef WIN32
  #ifdef bar_EXPORTS
    #define  BAR_EXPORT __declspec(dllexport)
  #else
    #define  BAR_EXPORT __declspec(dllimport)
  #endif
#else
 #define BAR_EXPORT
#endif

#endif /* BAR_EXPORT_H */
