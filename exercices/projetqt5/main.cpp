/*!
 * \file main.cpp
 * \brief Main for the example project
 * \author Alexandre Abadie
 * \version 0.1
 */

#include <QApplication>
#include "project.h"

 int main(int argc, char *argv[])
 {
     QApplication app(argc, argv);
     Project * p = new Project();
     p->show();
     return app.exec();
 }
