/*!
 * \file project.cpp
 * \brief Main class for the project application
 * \author Alexandre Abadie
 * \version 0.1
 */

#include "project.h"

Project::Project(QWidget *parent) : QMainWindow(parent)
{
  ui.setupUi(this);
}
