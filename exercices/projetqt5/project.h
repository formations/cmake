#ifndef PROJECT_H
#define PROJECT_H

/*!
 * \file project.h
 * \brief Main class for the project application
 * \author Alexandre Abadie
 * \version 0.1
 */

#include <QtGui>

#include "ui_project.h"


class Project : public QMainWindow
  {
    Q_OBJECT
  public:
    Ui::Project ui;
    Project(QWidget *parent = 0);
  };

#endif
