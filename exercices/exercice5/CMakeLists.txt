cmake_minimum_required (VERSION 2.8)

project(exercice5-cpack)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

add_executable(cmake-tutorial main.cpp)
install(TARGETS cmake-tutorial DESTINATION /bin) 

set(CPACK_GENERATOR "DEB")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Alexandre Abadie") #required

include(CPack)
