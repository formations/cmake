#ifndef FOO_H
#define FOO_H

#include <string>

#include "foo_export.h"

class FOO_EXPORT Foo
{

public:
    Foo() : m_description("description par defaut") {}

    ~Foo() {}

    void setDescription(const std::string & description);

    std::string description(void) const;

private:
    std::string m_description;
};

#endif
