#include <iostream>

#include "foo.h"

std::string Foo::description() const
{
    return m_description;
}

void Foo::setDescription(const std::string & description)
{
    m_description = description;
}
