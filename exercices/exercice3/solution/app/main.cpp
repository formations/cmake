#include <iostream>
#include "foo.h"

int main (int argc, char * argv[]) {
    (void) argc;
    (void) argv;
    std::cout << "Ma première application avec "
              << "CMake utilisant la lib foo" << std::endl;
    
    Foo foo;
    foo.setDescription("Nouvelle description de la lib foo");
    std::cout << foo.description() << std::endl;
    
    return 0;
}
