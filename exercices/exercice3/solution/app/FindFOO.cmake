#FindFOO.cmake
find_path(LIBFOO_INCLUDE_DIR foo.h
          HINTS /usr/include/foo
          PATH_SUFFIXES foo )

find_library(LIBFOO_LIBRARY foo
             HINTS /usr/lib)
