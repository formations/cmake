#include <iostream>

#include "foo.h"

int main (int argc, char * argv[]) {

    std::cout << "Ma première application avec "
              << "CMake utilisant la lib foo" << std::endl;

    Foo * foo = new Foo();
    foo->setDescription("Nouvelle description de la lib foo");
    std::cout << foo->description() << std::endl;

    delete foo;

    return 0;
}
