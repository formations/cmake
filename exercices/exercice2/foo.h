#include <string>

#include "foo_export.h"

class FOO_EXPORT Foo {

public:
    Foo() {};

    std::string description();
};
