#ifndef FOO_H
#define FOO_H

#include <string>

#include "foo_export.h"

class FOO_EXPORT Foo {

public:
    /**
     * @brief Foo contructor
     */
    Foo() {}
    
    /**
     * @brief Foo destructor
     */
    virtual ~Foo() {}

    /**
     * @brief description of foo
     * @return 
     */
    std::string description();
};

#endif
