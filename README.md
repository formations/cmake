## Support de cours de la formation CMake

Ce dépôt contient toutes les sources utilisées pour la formation sur CMake:
* les sources Latex des slides
* les codes sources des exercices
* les fichiers pdf des supports

Le support de cours au format pdf est téléchargeable [ici](cmake.pdf).

La fiche des exercices est téléchargable [ici](exercices.pdf).

Pour récupérer le code source des exercices, il suffit de clôner ce dépôt:
```
$ git clone git@gitlab.inria.fr:formations/cmake.git
```

Pour compiler les slides et le document des exercices à partir des sources:
```
$ make
```
